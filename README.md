#Dorothy - EffectEditor
&emsp;&emsp;这是一个使用Dorothy游戏框架开发，用于配合Dorothy框架使用的特效编辑器。在这里提供二进制可执行版本的下载。当前粒子特效的编辑功能已经完成，逐帧动画的编辑功能还在开发中。  
&emsp;&emsp;编辑器在初次运行后可以在Win下的`C:\Users\Username\AppData\Local\Dorothy\Effect`，或是在Mac下的`/Users/Username/Library/Caches/Effect`路径中找到编辑器资源的输入输出目录。  
&emsp;&emsp;生成的特效文件在Dorothy游戏框架中的使用方法：编辑器会产出以`.par`结尾的粒子特效文件，`.frame`结尾的逐帧动画文件，`main.effect`特效清单文件，以及其它引用的图片文件。将以上所有相关文件复制到可访问的程序资源目录下，然后在代码中引用和进行加载。示例的加载代码如下：  
```lua
setfenv(Dorothy())

local scene = CCScene()

oCache.Effect:load("main.effect") -- 通过特效清单文件加载所有的特效

oEffect("fire"):attachTo(scene):setOffset(400,400):autoRemove():start() -- 通过特效的名称创建特效并添加到场景中

CCDirector:run(scene)
```
&emsp;&emsp;该编辑器全部使用Dorothy框架的Lua接口提供的功能完成开发，所有的源码见`EffectEditor/Script`目录下的所有lua代码。了解Dorothy游戏框架：[这里](https://git.oschina.net/pig/Dorothy.git)。以下是部分编辑器的功能截图。  

![编辑器截图](http://git.oschina.net/pig/EffectEditor/raw/master/EffectEditor/EffectEditor.png)
